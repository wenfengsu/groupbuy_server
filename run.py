#!flask/bin/python
import os
from app import create_app
os.environ['DATABASE_URL'] = 'mysql://apps:apps@localhost/groupbuy'

app = create_app(os.getenv('FLASK_CONFIG') or 'default') 
print os.getenv('FLASK_CONFIG')

#app.run(debug=True, host='0.0.0.0')
if __name__ == '__main__':
	app.run()
#app.run()
