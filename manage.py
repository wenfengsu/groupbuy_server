#!/usr/bin/env python
import os

from app import create_app, db
from app.models import User, Role, Post
from flask.ext.script import Manager, Shell, Server
from flask.ext.migrate import Migrate, MigrateCommand



app = create_app(os.getenv('FLASK_CONFIG') or 'default') 
manager = Manager(app)
migrate = Migrate(app, db)




def make_shell_context():
	return dict(app=app, db=db, User=User, Role=Role, Post=Post)

manager.add_command("shell", Shell(make_context=make_shell_context))
manager.add_command('db', MigrateCommand)

if os.getenv('FLASK_CONFIG') is None:
        print 'running on dev'
        manager.add_command('runserver', Server(host="0.0.0.0", port=9000))


@manager.command
def test():
	"""Run the unit tests."""
	import unittest
	tests = unittest.TestLoader().discover('tests') 
	unittest.TextTestRunner(verbosity=2).run(tests)

@manager.command
def deploy():
    """Run deployment tasks."""
    from flask.ext.migrate import upgrade
    from app.models import Role, User

    # migrate database to latest revision
    upgrade()

    # create user roles
    Role.insert_roles()

    # create self-follows for all users
    User.add_self_follows()



if __name__ == '__main__': 
	manager.run()
