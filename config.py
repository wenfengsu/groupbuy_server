import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config:

	SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')
	#SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
    	SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')

	WHOOSH_BASE = os.path.join(basedir, 'search.db')

	WTF_CSRF_ENABLED = True
	SECRET_KEY = 'you-will-never-guess-my-secret-key'


	#if os.environ.get('DATABASE_URL') is None:
	#       SQLALCHEMY_DATABASE_URI = ('sqlite:///' + os.path.join(basedir, 'app.db') +
	#                               '?check_same_thread=False')
	#else:
	#SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
	#SQLALCHEMY_DATABASE_URI = 'mysql://apps:apps@localhost/groupbuy'

	SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')

	PRODUCT_IMG_DIR = os.path.join(basedir, 'app/static/media/product_images/')
	ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

	EMAIL_SERVER = {
                'server': 'smtp.zoho.com',
                'port': 465,
                'ssl' : True,
                'tls': False,
                'username': 'noreply@gogettr.com',
                'password': '28809938end'
                }

	# administrator list
	ADMINS = ['wenfeng.su@gmail.com']

	#pagination
	POSTS_PER_PAGE = 10
	FOLLOWERS_PER_PAGE = 20
	MAX_SEARCH_RESULTS = 50
	COMMENTS_PER_PAGE = 30
    
	@staticmethod
    	def init_app(app):
        	pass

class DevelopmentConfig(Config):
    DEBUG = True
    print "dev environment"
    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL')

class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DATABASE_URL')
    WTF_CSRF_ENABLED = False


class ProductionConfig(Config):
    print "production"
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')
    #SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'data.sqlite')
    print os.environ.get('DATABASE_URL')
    print os.environ.get('FLASK_CONFIG')

    @classmethod
    def init_app(cls, app):
        Config.init_app(app)


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig
}
