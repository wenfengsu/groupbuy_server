var myApp = angular.module('myApp', [
	'ngRoute',
	'myControllers',
	'myServices',
	'loginControllers',
	'userServices'
]);


myApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'static/angular/views/home/home.html',
        controller: 'HomeController'
      })
      .when('/login',{
	templateUrl: 'static/angular/views/login/login.html',
        controller: 'LoginController'
      })
      .otherwise({
        redirectTo: '/home'
      });
}]);
