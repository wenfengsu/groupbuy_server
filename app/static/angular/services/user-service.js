'use strict';

/* Services */

var userServices = angular.module('userServices', []);

userServices.factory('User', function(){
	function User() {	
		this.isLogged = null;
		this.username = null;
		this.avatar = null;
	}
	return (User);
});


userServices.factory('AuthService', [
	'$http',
	'$q',
	'User',	
	'Base64',
	function ($http, $q, User, Base64){

		var _setUserData = function (userData) {
			var _user = new User();
			_user.isLogged = true;
			_user.username = userData.username;
			_user.avatar = userData.avatar;
			return _user;
		}


		var _setUserLogOutData = function () {
                	var _user = new User();
                	angular.extend(_user, { isLogged: false })
                	return _user;
		}

		var _setToken = function (token) {
			if (!token) {
                    		localStorage.removeItem('token');
                	} else {
                    		localStorage.setItem('token', token);
                	}
		}


		var _getUserInfo = function () {
			_authService.getUserInfo();
		}


		var _authService = {

			user: angular.extend(new User(), { isLogged: false }),

			login: function (inputParams) {
				var config = {
                                	method: 'GET',
                                	url: '/api/v1.0/token',
                        	};

                        	$http.defaults.headers.common['Authorization'] = 'Basic ' + Base64.encode('wenfeng.su@gmail.com' + ':' + 'mypass');

                        	$http(config)
                        	.success(function(data, status, headers, config) {
					alert(JSON.stringify(data));
                                	if (data.token) {
						_setUserData(data);

						_setToken(data.token);
                                	}
                                	else {
                                        	User.isLogged = false;
                                        	User.username = '';
                                        	User.token = '';
                                        	User.avatar = '';
                                        	$location.path('/login');
                                	}
                        	})
                        	.error(function(data, status, headers, config) {
                                	User.isLogged = false;
                                	User.username = '';
                                	User.token = '';
                                	User.avatar = '';
                                	$location.path('/login');
                        	});

			},

        		logout: function(){
				 _setToken(null);
			},
        		getUserInfo:function(){
				var config = {
                                        method: 'GET',
                                        url: '/api/v1.0/users/myinfo',
                                };
				$http(config)
				.success(function(data, status, headers, config){
					var _user = _setUserData(data);
					angular.copy(_user, _authService.user);
					
				})
				.error(function(data, status, headers, config){
					alert('error');
					var _user = _setUserLogOutData();
                                	angular.copy(_user, _authService.user);
				});


			}
		}
	
        	var _token = localStorage.getItem('token');
        	if (_token) {
        		_getUserInfo();
       		}
        	return _authService;
       	}
]);



userServices.factory('UserAuthInterceptor', ['$q',
	function($q){
		return {
                	request: function (config) {
                    		config.headers = config.headers || {};
                    		var _token = localStorage.getItem('token');
                    		if (_token) {
                        		config.headers.Authorization = 'Bearer ' + _token;
                    		}
                    		return config || $q.when(config);
                	}
            	}
	}
]);




userServices.factory('Base64', function() {
    var keyStr = 'ABCDEFGHIJKLMNOP' +
            'QRSTUVWXYZabcdef' +
            'ghijklmnopqrstuv' +
            'wxyz0123456789+/' +
            '=';
    return {
        encode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;

            do {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);

                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;

                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }

                output = output +
                        keyStr.charAt(enc1) +
                        keyStr.charAt(enc2) +
                        keyStr.charAt(enc3) +
                        keyStr.charAt(enc4);
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
            } while (i < input.length);

            return output;
        },

        decode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;

            // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
            var base64test = /[^A-Za-z0-9\+\/\=]/g;
            if (base64test.exec(input)) {
                alert("There were invalid base64 characters in the input text.\n" +
                        "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                        "Expect errors in decoding.");
            }
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

            do {
                enc1 = keyStr.indexOf(input.charAt(i++));
                enc2 = keyStr.indexOf(input.charAt(i++));

		enc3 = keyStr.indexOf(input.charAt(i++));
                enc4 = keyStr.indexOf(input.charAt(i++));

                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;

                output = output + String.fromCharCode(chr1);

                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }

                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";

            } while (i < input.length);

            return output;
        }
    };
});

