'use strict';

/* Services */

var myServices = angular.module('myServices', ['ngResource']);

myServices.factory('Post', ['$resource', 
	function($resource){
		return $resource("/api/v1.0/posts/:postId", {}, {
			query: {method:'GET', params:{postId:''}, isArray:false}
		});
	}]);


