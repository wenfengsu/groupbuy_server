"use strict";

var loginControllers = angular.module('loginControllers', []);


loginControllers.controller('LoginController', ['$scope', '$http', '$location', 'AuthService',
        function ($scope, $http, $location, AuthService) {
		$scope.login = function() {
			AuthService.login(null);
		}

	}]
);
