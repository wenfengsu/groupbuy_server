"use strict";

var myControllers = angular.module('myControllers', []);

myControllers.controller('HomeController', ['$scope', 'Post', 'AuthService',
	function ($scope, Post, AuthService) {
		Post.query(function(data){
			$scope.posts = data.posts;

			$scope.user = AuthService.user;
		});

	}]);
