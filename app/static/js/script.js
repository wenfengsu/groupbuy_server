
$(document).ready(function () {

    $("#imageupload").change(function() {
	var all_files = this.files;

        for (var i = 0; i < all_files.length; ++i) {
  		var file = all_files.item(i);
		var preview = createPreviews(file.name, i+1);
		uploadImage(file, i+1, preview);
	}
    });

    $("#SubmitPostFormButton").click(function(e){
	e.preventDefault();
	var formdata = new FormData();

	body = $('#post-body').val();
	images = $('#post-images').val();
	title = $('#post-title').val();	

        formdata.append('post-body', body);
        formdata.append('post-images', images);
	formdata.append('post-title', title);



	$.ajax({
                url: '/submitpost',  //Server script to process data
                type: 'POST',
                dataType: 'json',

                success: postSubmitSuccessed,
                error: errorHandler,
                data: formdata,
                //Options to tell jQuery not to process data or worry about content-type.
                contentType: false,
                processData: false
        });
    });


    $(".want-or-not").click(function(e){
	e.preventDefault();
	url = $(this).attr('href');
	post = $(this).parent().parent();
	$.ajax({
                url: url, 
                type: 'GET',
                dataType: 'json',
		post: post,
                success: function(data){
			count = data['users_wanted'];
			enable = data['enable'];

			if (count > 1){
				post.find('.people-want').html("<span>"+data['users_wanted']+" people want it.</span>");
				post.find('.people-want').show();
			}else if (count > 0){
				post.find('.people-want').html("<span>"+data['users_wanted']+" person want it.</span>");
                                post.find('.people-want').show();
			}else{
				post.find('.people-want').hide();
			}

			if(enable == "Yes"){
				post.find('.want-or-not span').removeClass('label-danger').addClass('label-primary');
				post.find('.want-or-not span').text("I want this ("+data['users_wanted']+")");
			}else{
				post.find('.want-or-not span').removeClass('label-primary').addClass('label-danger');
				post.find('.want-or-not span').text("I don't want this ("+data['users_wanted']+")");
			}

		},
                error: function(e){
			alert('error');
		},
        });

	
    });

});


function postSubmitSuccessed(data){
	html = '<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert">&times;</button>Product submitted successfully.</div>';
	$('.container.main-body').prepend(html);


	$('#post-body, #post-images').val('');
	$('.images-preview div').not('.img-preview').remove();


}

function createPreviews(filename, sequence){
	newPreview = $('.img-preview').clone();
	newPreview.removeClass('img-preview');
	$('.images-preview').append(newPreview);

	return newPreview;
}


function uploadImage(image, sequence, preview){

	var formdata = new FormData();
        formdata.append('file', image);
	formdata.append('sequence', sequence);

	$.ajax({
        	url: '/upload',  //Server script to process data
        	type: 'POST',
		dataType: 'json',
		preview: preview,
        	xhr: function() {  // Custom XMLHttpRequest
            		var myXhr = $.ajaxSettings.xhr();
            		if(myXhr.upload){ // Check if upload property exists
				myXhr.upload.preview = preview;
                		myXhr.upload.addEventListener('progress', function(e){
					var percent = parseInt(e.loaded / e.total * 100);
					console.log(percent);

					this.preview.find(".progress-bar").css("width", percent+"%").attr('aria-valuenow', percent);
					this.preview.find(".progress-bar").text(percent);
	
				}, false); // For handling the progress of the upload

				myXhr.upload.addEventListener("load", function(e){
					console.log("upload complete");
					console.log(e);
				}, false);

           		}
            		return myXhr;
        	},
        	//Ajax events
        	beforeSend: beforeSendHandler,
        	success: function(data){
			imagesTextAreaVal = $('#post-images').val();
        		if (imagesTextAreaVal == ""){
                		imagesTextAreaVal = data['filename']+':'+data['sequence'];
        		}else{
                		imagesTextAreaVal = imagesTextAreaVal+';'+data['filename']+':'+data['sequence'];
        		}

        		$('#post-images').val(imagesTextAreaVal);

			preview.find('a img').attr('src', '/static/media/product_images/'+data['filename']);
			preview.find('.progress').hide();
		},
        	error: errorHandler,
        	// Form data
        	data: formdata,
        	//Options to tell jQuery not to process data or worry about content-type.
        	cache: false,
        	contentType: false,
        	processData: false
    	});
}

function beforeSendHandler(){
	/*alert('before');*/
}

function errorHandler(){
	/*alert('error');*/
}


/*function progressHandlingFunction(e){
    if(e.lengthComputable){
	var percentComplete = parseInt(e.loaded / e.total * 100);
	console.log(percentComplete);
	
    }else{
	console.log("can't complete");
    }
}
*/
