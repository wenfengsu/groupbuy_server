from flask import jsonify, request, current_app, url_for, g
from . import api
from ..models import User, Post, Permission, Wantit
from .decorators import permission_required
from .. import db
from ..emails import send_email
from .authentication import get_token, verify_password
from flask.ext.login import login_user



@api.route('/users/myinfo/')
def my_info():
    #return jsonify(g.current_user.to_json())
    return jsonify({'username': g.current_user.username, 'avatar': g.current_user.avatar(size=20)})


@api.route('/users/<int:id>')
def get_user(id):
    user = User.query.get_or_404(id)
    return jsonify(user.to_json())

# this is a private helper method
def get_users():
        page = request.args.get('page', 1, type=int)
        pagination = User.query.paginate(page, per_page=current_app.config['POSTS_PER_PAGE'],error_out=False)
        users = pagination.items
        prev = None
        if pagination.has_prev:
                prev = url_for('api.get_users', page=page-1, _external=True)
        next = None
        if pagination.has_next:
                next = url_for('api.get_users', page=page+1, _external=True)
        return jsonify({
                'users': [user.to_json() for user in users],
                'prev': prev,
                'next': next,
                'count': pagination.total
                })


@api.route('/users/<int:id>/products/')
def get_user_posts(id):
    user = User.query.get_or_404(id)
    page = request.args.get('page', 1, type=int)
    pagination = user.posts.order_by(Post.timestamp.desc()).paginate(
        page, per_page=current_app.config['POSTS_PER_PAGE'],
        error_out=False)
    posts = pagination.items
    prev = None
    if pagination.has_prev:
        prev = url_for('api.get_posts', page=page-1, _external=True)
    next = None
    if pagination.has_next:
        next = url_for('api.get_posts', page=page+1, _external=True)
    return jsonify({
        'posts': [post.to_json() for post in posts],
        'prev': prev,
        'next': next,
        'count': pagination.total
    })

@api.route('/users/<int:id>/products_wanted/')
def posts_wanted(id):
    user = User.query.get_or_404(id)
    page = request.args.get('page', 1, type=int)


    pagination = user.products_wanted.order_by(Post.timestamp.desc()).paginate(
        page, per_page=current_app.config['POSTS_PER_PAGE'],
        error_out=False)
    posts = pagination.items
    prev = None
    if pagination.has_prev:
        prev = url_for('api.get_posts', page=page-1, _external=True)
    next = None
    if pagination.has_next:
        next = url_for('api.get_posts', page=page+1, _external=True)
    return jsonify({
        'posts': [post.to_json() for post in posts],
        'prev': prev,
        'next': next,
        'count': pagination.total
    })

@api.route('/users/<int:id>/timeline/')
def get_user_followed_posts(id):
    user = User.query.get_or_404(id)
    page = request.args.get('page', 1, type=int)
    pagination = user.followed_posts.order_by(Post.timestamp.desc()).paginate(
        page, per_page=current_app.config['POSTS_PER_PAGE'],
        error_out=False)
    posts = pagination.items
    prev = None
    if pagination.has_prev:
        prev = url_for('api.get_users', page=page-1, _external=True)
    next = None
    if pagination.has_next:
        next = url_for('api.get_users', page=page+1, _external=True)
    return jsonify({
        'posts': [post.to_json() for post in posts],
        'prev': prev,
        'next': next,
        'count': pagination.total
    })


@api.route('/users/want-or-not/<int:postid>', methods=['GET'])
@permission_required(Permission.FOLLOW)
def want_or_not(postid):
	post = Post.query.get_or_404(postid)
        if post is not None:
        	g.current_user.want_or_dont_want_post(post)
               	users_wanted_count = post.buyers.count()
                if g.current_user.already_want(post):
                	return jsonify({'users_wanted': users_wanted_count, 'enable': 'No'})
              	else:
                	return jsonify({'users_wanted': users_wanted_count, 'enable': 'Yes'})

@api.route('/users/<int:id>/followers', methods=['GET'])
def followers(id):
    user = User.query.get_or_404(id)
    page = request.args.get('page', 1, type=int)
    pagination = user.followers.paginate(
        page, per_page=current_app.config['FOLLOWERS_PER_PAGE'],
        error_out=False)

    prev = None
    if pagination.has_prev:
        prev = url_for('api.get_posts', page=page-1, _external=True)
    next = None
    if pagination.has_next:
        next = url_for('api.get_posts', page=page+1, _external=True)
    return jsonify({
        'followers': [{
                'user': item.follower.to_json(),
                'timestamp': item.timestamp}
               for item in pagination.items],
        'prev': prev,
        'next': next,
        'count': pagination.total
    })




@api.route('/users/<int:id>/followed_by', methods=['GET'])
def followed_by(id):
    user = User.query.get_or_404(id)
    page = request.args.get('page', 1, type=int)
    pagination = user.followed.paginate(
        page, per_page=current_app.config['FOLLOWERS_PER_PAGE'],
        error_out=False)


    prev = None
    if pagination.has_prev:
        prev = url_for('api.get_posts', page=page-1, _external=True)
    next = None
    if pagination.has_next:
        next = url_for('api.get_posts', page=page+1, _external=True)
    return jsonify({
        'followed_by': [{
                'user': item.followed.to_json(),
                'timestamp': item.timestamp}
               for item in pagination.items],
        'prev': prev,
        'next': next,
        'count': pagination.total
    })

@api.route('/users/', methods=['POST'])
def new_user():
	user = User.from_json(request.json)

	if user is not None:
		# this is for register with GB account user (email confirmation), facebook/google login would be automatically confirmed
		if user.confirmed != True:
			token = user.generate_confirmation_token()
			send_email(user.email, 'Confirm Your Account', 'auth/email/confirm', user=user, token=token)
			return jsonify({'status': 'ConfirmationSent', 'email': 'A confirmation email has been sent to you by email.',})

		login_user(user, True)
		g.current_user = user

		return jsonify({'status': 'Successed', 'token': g.current_user.generate_auth_token(), 'expiration': 0})
	else:
		return jsonify({'status': 'Failed'})
