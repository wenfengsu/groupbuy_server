from flask import jsonify, request, g, abort, url_for, current_app
from .. import db
from ..models import Post, Permission, Wantit
from . import api
from .decorators import permission_required
from .errors import forbidden

from werkzeug import secure_filename
import hashlib
import time
import json


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in current_app.config['ALLOWED_EXTENSIONS']


@api.route('/images/', methods=['POST'])
@permission_required(Permission.WRITE_ARTICLES)
def images():
    	file = request.files.get('file')
        sequence = request.form['sequence']
        email_hash = hashlib.md5(current_user.email).hexdigest()

        #for file in files:
        result = dict()

        if file and allowed_file(file.filename):

                timestamp = str(int(time.time()))
                # Make the filename safe, remove unsupported chars
                filename = secure_filename(file.filename)
                filename = email_hash+'-'+timestamp+'-'+filename

                # Move the file form the temporal folder to
                # the upload folder we setup
                file.save(os.path.join(current_app.config['PRODUCT_IMG_DIR'], filename))

                #product_image = ProductImage(filename=filename, order=1, post=post)
                #db.session.add(product_image)
                #db.session.commit()
                result['filename'] = filename
                result['sequence'] = sequence
		return json.dumps(result)

        return None
