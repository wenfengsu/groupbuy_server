from flask import render_template, current_app
from flask.ext.mail import Message
from .decorators import async

import smtplib
from smtplib import SMTPException
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

@async
def send_async_email(app, msg):
    with app.app_context():
        server = smtplib.SMTP_SSL(app.config['EMAIL_SERVER']['server'], app.config['EMAIL_SERVER']['port'])
        server.login(app.config['EMAIL_SERVER']['username'], app.config['EMAIL_SERVER']['password'])
        server.sendmail(msg['From'], msg['To'], msg.as_string())
        server.quit()


def send_email(to, subject, template, **kwargs):
    app = current_app._get_current_object()

    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['From'] = app.config['EMAIL_SERVER']['username']
    msg['To'] = to

    body_text = MIMEText(render_template(template + '.txt', **kwargs), 'plain')
    body_html = MIMEText(render_template(template + '.html', **kwargs), 'html')
    msg.attach(body_text)
    msg.attach(body_html)


    print msg

    send_async_email(app, msg)


def follower_notification(followed, follower):
    send_email("[microblog] %s is now following you!" % follower.username,
               current_app.config['ADMINS'][0],
               [followed.email],
               render_template("follower_email.txt",
                               user=followed, follower=follower),
               render_template("follower_email.html",
                               user=followed, follower=follower))
