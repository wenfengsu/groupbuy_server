from flask.ext.wtf import Form
from wtforms import StringField, BooleanField, TextAreaField, PasswordField, SubmitField, SelectField, FileField


from wtforms.validators import DataRequired, Length, Required, Email, Regexp, EqualTo
from wtforms import ValidationError 
from ..models import User, Role

class EditForm(Form):
    username = StringField('username', validators=[DataRequired()])
    about_me = TextAreaField('about_me', validators=[Length(min=0, max=140)])

    def __init__(self, original_username, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        self.original_username = original_username

    def validate(self):
        if not Form.validate(self):
            return False
        if self.username.data == self.original_username:
            return True
        user = User.query.filter_by(username=self.username.data).first()
        if user is not None:
            self.username.errors.append('This username is already in use. '
                                        'Please choose another one.')
            return False
        return True


class PostForm(Form):
    title = StringField('Title', validators=[Length(0, 64), Required()])
    body = TextAreaField("Sell something locally?", validators=[Required()])
    images = TextAreaField()
    submit = SubmitField('Submit')

class SearchForm(Form):
    search = StringField('search', validators=[DataRequired()])


class EditProfileForm(Form):
    name = StringField('Real name', validators=[Length(0, 64)])
    location = StringField('Location', validators=[Length(0, 64)])
    about_me = TextAreaField('About me')
    submit = SubmitField('Submit')


class EditProfileAdminForm(Form):
    email = StringField('Email', validators=[Required(), Length(1, 64),Email()])
    username = StringField('Username', validators=[
        Required(), Length(1, 64), Regexp('^[A-Za-z][A-Za-z0-9_.]*$', 0,
                                          'Usernames must have only letters, '
                                          'numbers, dots or underscores')])
    confirmed = BooleanField('Confirmed')
    role = SelectField('Role', coerce=int)
    name = StringField('Real name', validators=[Length(0, 64)])
    location = StringField('Location', validators=[Length(0, 64)])
    about_me = TextAreaField('About me')
    submit = SubmitField('Submit')

    def __init__(self, user, *args, **kwargs): 
	super(EditProfileAdminForm, self).__init__(*args, **kwargs) 
	self.role.choices = [(role.id, role.name)
		for role in Role.query.order_by(Role.name).all()]
        self.user = user

    def validate_email(self, field):
	if field.data != self.user.email and \
			User.query.filter_by(email=field.data).first(): 
		raise ValidationError('Email already registered.')

    def validate_username(self, field):
	if field.data != self.user.username and \
			User.query.filter_by(username=field.data).first(): 
		raise ValidationError('Username already in use.')


class CommentForm(Form):
	body = StringField('', validators=[Required()]) 
	submit = SubmitField('Submit')

