import os
from flask import render_template, flash, redirect, session, url_for, request, g, abort, send_from_directory, jsonify
from flask.ext.login import login_user, logout_user, current_user, login_required
from datetime import datetime
from flask import current_app
from werkzeug import secure_filename
import hashlib, time, json, re

from . import main
from .forms import EditForm, PostForm, SearchForm, EditProfileForm, EditProfileAdminForm, CommentForm
from .. import db
from ..models import User, Post, Role, Permission, Comment, ProductImage, Tag
from ..decorators import admin_required, permission_required



def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in current_app.config['ALLOWED_EXTENSIONS']


@main.route('/upload', methods=['GET', 'POST'])
def upload():

	file = request.files.get('file')
	sequence = request.form['sequence']
        email_hash = hashlib.md5(current_user.email).hexdigest()
        #for file in files:
	result = dict()

        if file and allowed_file(file.filename):
		
                timestamp = str(int(time.time()))
                # Make the filename safe, remove unsupported chars
                filename = secure_filename(file.filename)
              	filename = email_hash+'-'+timestamp+'-'+filename

                # Move the file form the temporal folder to
                # the upload folder we setup
                file.save(os.path.join(current_app.config['PRODUCT_IMG_DIR'], filename))

                #product_image = ProductImage(filename=filename, order=1, post=post)
                #db.session.add(product_image)
		#db.session.commit()
		result['filename'] = filename
		result['sequence'] = sequence
		return json.dumps(result)


	return None

@main.route('/submitpost', methods=['POST'])
def submitPost():
	body = request.form['post-body']

	print 'hello**************'
	##############	
	pattern = re.compile(r"#(\w+)")
	hashtags = pattern.findall(body)
	#print hashtags
	
	dollar_pat = re.compile(r"\$([0-9]+)")
	dollar_tags = dollar_pat.findall(body)

	#print dollar_tags

	if len(dollar_tags) == 0:
		return	

	print 'test'
	title = request.form['post-title']
	if len(title) == 0:
		return
	price = dollar_tags[0]
	
	print title


	#print dollar_tags


	##############
	
	images_string = request.form['post-images']

	if price is not None:
		post = Post(body=body, title=title, author=current_user._get_current_object(), price=price)
        	db.session.add(post)
	else:
		flash('the post has to have a price tag')
		return

	if len(hashtags)>0:
		Tag.add_hash_tags(hashtags, post)		

	result = dict()
	print images_string

       	images = images_string.split(";")
        for img in images:
        	parts = img.split(':')
        	if len(parts) is not None:
                	filename = parts[0]
                	sequence = parts[1]
                     	product_image = ProductImage(filename=filename, order=sequence, post=post)
                    	db.session.add(product_image)

       	db.session.commit()
	result['Response'] = 'SUCCESSED'

       	return json.dumps(result)


@main.route('/', methods=['GET', 'POST'])
@main.route('/index', methods=['GET', 'POST'])
def index():

	"""page = request.args.get('page', 1, type=int)
    	pagination = Post.query.order_by(Post.timestamp.desc()).paginate(
        		page, per_page=current_app.config['POSTS_PER_PAGE'],
			error_out=False)
	posts = pagination.items
	return render_template('index.html', posts=posts, pagination=pagination)
	"""
	return current_app.send_static_file('index.html')

@main.route('/user/<username>') 
def user(username):
	user = User.query.filter_by(username=username).first() 
	if user is None:
		abort(404)

	page = request.args.get('page', 1, type=int)
        pagination = user.posts.order_by(Post.timestamp.desc()).paginate(
                        page, per_page=current_app.config['POSTS_PER_PAGE'],
                        error_out=False)
        posts = pagination.items

	return render_template('user.html', user=user, posts=posts, pagination=pagination)

@main.route('/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit(id):
    post = Post.query.get_or_404(id)
    if current_user != post.author and \
            not current_user.can(Permission.ADMINISTER):
        abort(403)
    form = PostForm()
    if form.validate_on_submit():
        post.body = form.body.data
        db.session.add(post)
        flash('The post has been updated.')
        return redirect(url_for('.post', id=post.id))
    form.body.data = post.body
    return render_template('edit_post.html', form=form)


@main.route('/edit-profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
	form = EditProfileForm()
	if form.validate_on_submit():
		current_user.name = form.name.data
		current_user.location = form.location.data
		current_user.about_me = form.about_me.data
		db.session.add(current_user)
		db.session.commit()
		flash('Your profile has been updated.')
		return redirect(url_for('.user', username=current_user.username))
	form.name.data = current_user.name
	form.location.data = current_user.location
	form.about_me.data = current_user.about_me
	return render_template('edit_profile.html', form=form)


@main.route('/edit-profile/<int:id>', methods=['GET', 'POST']) 
@login_required
@admin_required
def edit_profile_admin(id):
	user = User.query.get_or_404(id)
	form = EditProfileAdminForm(user=user) 
	if form.validate_on_submit():
        	user.email = form.email.data
        	user.username = form.username.data
        	user.confirmed = form.confirmed.data
       
		user.role = Role.query.get(form.role.data)
        	user.name = form.name.data
        	user.location = form.location.data
        	user.about_me = form.about_me.data
        	db.session.add(user)
		db.session.commit()
        	flash('The profile has been updated.')
		return redirect(url_for('.user', username=user.username)) 
	form.email.data = user.email
	form.username.data = user.username
	form.confirmed.data = user.confirmed
	form.role.data = user.role_id
	form.name.data = user.name
	form.location.data = user.location
	form.about_me.data = user.about_me
	return render_template('edit_profile.html', form=form, user=user)






@main.route('/follow/<username>')
@login_required
@permission_required(Permission.FOLLOW)
def follow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('Invalid user.')
        return redirect(url_for('.index'))
    if current_user.is_following(user):
        flash('You are already following this user.')
        return redirect(url_for('.user', username=username))
    current_user.follow(user)
    flash('You are now following %s.' % username)
    return redirect(url_for('.user', username=username))


@main.route('/unfollow/<username>')
@login_required
@permission_required(Permission.FOLLOW)
def unfollow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('Invalid user.')
        return redirect(url_for('.index'))
    if not current_user.is_following(user):
        flash('You are not following this user.')
        return redirect(url_for('.user', username=username))
    current_user.unfollow(user)
    flash('You are not following %s anymore.' % username)
    return redirect(url_for('.user', username=username))



@main.route('/followers/<username>')
def followers(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('Invalid user.')
        return redirect(url_for('.index'))
    page = request.args.get('page', 1, type=int)
    pagination = user.followers.paginate(
        page, per_page=current_app.config['FOLLOWERS_PER_PAGE'],
        error_out=False)
    follows = [{'user': item.follower, 'timestamp': item.timestamp}
               for item in pagination.items]
    return render_template('followers.html', user=user, title="Followers of",
                           endpoint='.followers', pagination=pagination,
                           follows=follows)


@main.route('/followed-by/<username>')
def followed_by(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('Invalid user.')
        return redirect(url_for('.index'))
    page = request.args.get('page', 1, type=int)
    pagination = user.followed.paginate(
        page, per_page=current_app.config['FOLLOWERS_PER_PAGE'],
        error_out=False)
    follows = [{'user': item.followed, 'timestamp': item.timestamp}
               for item in pagination.items]
    return render_template('followers.html', user=user, title="Followed by",
                           endpoint='.followed_by', pagination=pagination,
                           follows=follows)









#@main.route('/follow')
#def follow(username):
#	return

#@main.route('/unfollow')
#def unfollow(username):
#	return



@main.route('/post/<int:id>', methods=['GET', 'POST'])
def post(id):
    post = Post.query.get_or_404(id)
    form = CommentForm()
    if form.validate_on_submit():
        comment = Comment(body=form.body.data,
                          post=post,
                          author=current_user._get_current_object())
        db.session.add(comment)
	db.session.commit()
        flash('Your comment has been published.')
        return redirect(url_for('.post', id=post.id, page=-1))
    page = request.args.get('page', 1, type=int)
    if page == -1:
        page = (post.comments.count() - 1) / \
            current_app.config['COMMENTS_PER_PAGE'] + 1

    pagination = post.comments.order_by(Comment.timestamp.asc()).paginate(
        page, per_page=current_app.config['COMMENTS_PER_PAGE'],
        error_out=False)
    comments = pagination.items
    return render_template('post.html', posts=[post], form=form,
                           comments=comments, pagination=pagination)

@main.route('/want-or-not/<int:id>')
@login_required
@permission_required(Permission.FOLLOW)
def want_or_not(id):
	post = Post.query.get_or_404(id)
	if post is not None:
		current_user.want_or_dont_want_post(post)
		users_wanted_count = post.buyers.count()
		if current_user.already_want(post):
			return jsonify({'users_wanted': users_wanted_count, 'enable': 'No'})
		else:
			return jsonify({'users_wanted': users_wanted_count, 'enable': 'Yes'})

	return redirect(url_for('.index'))




@main.route('/moderate')
@login_required
@permission_required(Permission.MODERATE_COMMENTS)
def moderate():
    page = request.args.get('page', 1, type=int)
    pagination = Comment.query.order_by(Comment.timestamp.desc()).paginate(
        page, per_page=current_app.config['COMMENTS_PER_PAGE'],
        error_out=False)
    comments = pagination.items
    return render_template('moderate.html', comments=comments,
                           pagination=pagination, page=page)


@main.route('/moderate/enable/<int:id>')
@login_required
@permission_required(Permission.MODERATE_COMMENTS)
def moderate_enable(id):
    comment = Comment.query.get_or_404(id)
    comment.disabled = False
    db.session.add(comment)
    db.session.commit()
    return redirect(url_for('.moderate',
                            page=request.args.get('page', 1, type=int)))


@main.route('/moderate/disable/<int:id>')
@login_required
@permission_required(Permission.MODERATE_COMMENTS)
def moderate_disable(id):
    comment = Comment.query.get_or_404(id)
    comment.disabled = True
    db.session.add(comment)
    db.session.commit()
    return redirect(url_for('.moderate',
                            page=request.args.get('page', 1, type=int)))

