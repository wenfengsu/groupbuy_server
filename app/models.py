import hashlib
from app import db
from flask.ext.login import UserMixin, AnonymousUserMixin
from . import lm
from werkzeug.security import generate_password_hash, check_password_hash

import bleach
from markdown import markdown
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer 
from flask import current_app, request, url_for
from datetime import datetime
from app.exceptions import ValidationError
import requests, json
from sqlalchemy import and_


import sys


if sys.version_info >= (3, 0):
    enable_search = False
else:
    enable_search = True
    import flask.ext.whooshalchemy as whooshalchemy


class Permission: 
    FOLLOW = 0x01
    COMMENT = 0x02
    WRITE_ARTICLES = 0x04
    MODERATE_COMMENTS = 0x08
    ADMINISTER = 0x80


class Follow(db.Model):
    __tablename__ = 'follows'
    follower_id = db.Column(db.Integer, db.ForeignKey('users.id'),
                            primary_key=True)
    followed_id = db.Column(db.Integer, db.ForeignKey('users.id'),
                            primary_key=True)
    timestamp = db.Column(db.DateTime, default=datetime.utcnow)


class Wantit(db.Model):
    __tablename__ = 'wantit'
    post_id = db.Column(db.Integer, db.ForeignKey('posts.id'),
                            primary_key=True)

    buyer_id = db.Column(db.Integer, db.ForeignKey('users.id'),
                            primary_key=True)

    timestamp = db.Column(db.DateTime, default=datetime.utcnow)


#Login manager call back function
@lm.user_loader
def load_user(user_id):
	return User.query.get(int(user_id))


class Posttag(db.Model):
        __tablename__ = 'posttags'
        post_id = db.Column(db.Integer, db.ForeignKey('posts.id'),
                            primary_key=True)

        tag_id = db.Column(db.Integer, db.ForeignKey('tags.id'),
                            primary_key=True)



class Tag(db.Model):
	__tablename__ = 'tags'
	id = db.Column(db.Integer, primary_key=True)
	tag = db.Column(db.String(64), unique=True, index=True)


	tag_posts = db.relationship('Posttag',
                               foreign_keys=[Posttag.tag_id],
                               backref=db.backref('tag', lazy='joined'),
                               lazy='dynamic',
                               cascade='all, delete-orphan')


	def __repr__(self):
                return self.tag


	def to_json(self):
        	tag = {"tag":self.tag}
        	return tag


	@staticmethod
    	def add_hash_tags(hashtags, post):
		for tag_string in hashtags:
			tag = Tag.query.filter_by(tag=tag_string).first()

			if tag is None:
				tag = Tag(tag=tag_string)
				db.session.add(tag)
				db.session.commit()
			#print tag.id, post.id

			post_tag = Posttag(post_id=post.id, tag_id=tag.id)				
			db.session.add(post_tag)
			db.session.commit()


class Role(db.Model):
	__tablename__ = 'roles'
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(64), unique=True)
	default = db.Column(db.Boolean, default=False, index=True)
	permissions = db.Column(db.Integer)
    	users = db.relationship('User', backref='role', lazy='dynamic')


	@staticmethod
	def insert_roles(): 
		roles = {
            		'User': (Permission.FOLLOW |
                     		Permission.COMMENT |
                     		Permission.WRITE_ARTICLES, True),
            		'Moderator': (Permission.FOLLOW |
                         	Permission.COMMENT |
                          	Permission.WRITE_ARTICLES |
                          	Permission.MODERATE_COMMENTS, False),
            		'Administrator': (0xff, False)
        	}

		for r in roles:
			role = Role.query.filter_by(name=r).first() 
			if role is None:
            			role = Role(name=r)
            		role.permissions = roles[r][0]
            		role.default = roles[r][1]
            		db.session.add(role)
        	db.session.commit()

	def __repr__(self):
        	return '<Role %r>' % self.name

class AccountProvider(db.Model):
    __tablename__ = 'accountprovider'
    id = db.Column(db.Integer, primary_key=True)
    owner_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    provider = db.Column(db.String(64), index=True)
    provider_user_id_hash = db.Column(db.String(128))
    provider_user_access_token_hash = db.Column(db.String(128))

    def __repr__(self):
                return self.provider


class User(UserMixin, db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    name = db.Column(db.String(64))

    location = db.Column(db.String(64))
    email = db.Column(db.String(120), index=True, unique=True)
    about_me = db.Column(db.String(140))
    last_seen = db.Column(db.DateTime)
    password_hash = db.Column(db.String(128))
    member_since = db.Column(db.DateTime(), default=datetime.utcnow)
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))
    confirmed = db.Column(db.Boolean, default=False)
    avatar_hash = db.Column(db.String(32))    
    posts = db.relationship('Post', backref='author', lazy='dynamic')
    comments = db.relationship('Comment', backref='author', lazy='dynamic')
    providers = db.relationship('AccountProvider', backref='owner', lazy='dynamic')

    followed = db.relationship('Follow',
                               foreign_keys=[Follow.follower_id],
                               backref=db.backref('follower', lazy='joined'),
                               lazy='dynamic',
                               cascade='all, delete-orphan')
    followers = db.relationship('Follow',
                                foreign_keys=[Follow.followed_id],
                                backref=db.backref('followed', lazy='joined'),
                                lazy='dynamic',
                                cascade='all, delete-orphan')


    posts_want = db.relationship('Wantit',
                               foreign_keys=[Wantit.buyer_id],
                               backref=db.backref('buyer', lazy='joined'),
                               lazy='dynamic',
                               cascade='all, delete-orphan')


    @staticmethod
    def add_self_follows():
        for user in User.query.all():
            if not user.is_following(user):
                user.follow(user)
                db.session.add(user)
                db.session.commit()




    def __init__(self, **kwargs): 
	super(User, self).__init__(**kwargs) 
	if self.role is None:
		if self.email == current_app.config['ADMINS'][0]:
			self.role = Role.query.filter_by(permissions=0xff).first()
		if self.role is None:
			self.role = Role.query.filter_by(default=True).first()


	if self.email is not None and self.avatar_hash is None:
		self.avatar_hash = hashlib.md5(self.email.encode('utf-8')).hexdigest()


   
    @staticmethod
    def generate_fake(count=100):
	from sqlalchemy.exc import IntegrityError 
	from random import seed
	import forgery_py

	seed()
	for i in range(count):
            u = User(email=forgery_py.internet.email_address(),
                     username=forgery_py.internet.user_name(True),
                     password=forgery_py.lorem_ipsum.word(),
                     confirmed=True,
                     name=forgery_py.name.full_name(),
                     location=forgery_py.address.city(),
                     about_me=forgery_py.lorem_ipsum.sentence(),
                     member_since=forgery_py.date.date(True))
	    db.session.add(u) 
	    try:
	    	db.session.commit() 
	    except IntegrityError:
                db.session.rollback()



    def change_email(self, token):
	self.email = new_email
	self.avatar_hash = hashlib.md5(
		self.email.encode('utf-8')).hexdigest()
	db.session.add(self)
	return True

    @staticmethod
    def make_unique_username(username):
        if User.query.filter_by(username=username).first() is None:
            return username
        version = 2
        while True:
            new_username = username + str(version)
            if User.query.filter_by(username=new_username).first() is None:
                break
            version += 1
        return new_username

    @property
    def password(self):
	raise AttributeError('password is not a readable attribute')
    
    @password.setter
    def password(self, password):
	self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
	return check_password_hash(self.password_hash, password)

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        try:
            return unicode(self.id)  # python 2
        except NameError:
            return str(self.id)  # python 3

    def avatar(self, size=100, default='identicon', rating='g'):
	if request.is_secure:
		url = 'https://secure.gravatar.com/avatar' 
	else:
		url = 'http://www.gravatar.com/avatar'
	hash = self.avatar_hash or hashlib.md5(self.email.encode('utf-8')).hexdigest() 
	return '{url}/{hash}?s={size}&d={default}&r={rating}'.format(
            url=url, hash=hash, size=size, default=default, rating=rating)

    def follow(self, user):
        if not self.is_following(user):
            f = Follow(follower=self, followed=user)
            db.session.add(f)
	    db.session.commit()

    def unfollow(self, user):
        f = self.followed.filter_by(followed_id=user.id).first()
        if f:
            db.session.delete(f)
	    db.session.commit()

    def is_following(self, user):
        return self.followed.filter_by(
            followed_id=user.id).first() is not None

    def is_followed_by(self, user):
        return self.followers.filter_by(
            follower_id=user.id).first() is not None

    def is_own_post(self, post):
        return self.posts.filter_by(id = post.id).first() is not None

    # this is a helper function for Views
    def already_want(self, post):
        return self.posts_want.filter_by(post_id=post.id).first() is not None

    def want_or_dont_want_post(self, post):

	if self.is_own_post(post):
		return

	w = self.posts_want.filter_by(post_id=post.id).first()

	if w is None:
	    w = Wantit(buyer_id=self.id, post_id=post.id)
	    db.session.add(w)
	else:
            db.session.delete(w)
	
	db.session.commit()

    @property
    def followed_posts(self):
        return Post.query.join(Follow, Follow.followed_id == Post.author_id).filter(Follow.follower_id == self.id)

    @property
    def products_wanted(self):
	return Post.query.join(Wantit, Wantit.post_id == Post.id).filter(Wantit.buyer_id == self.id)


    def generate_confirmation_token(self, expiration=3600):
	s = Serializer(current_app.config['SECRET_KEY'], expiration)
	return s.dumps({'confirm': self.id})

    def confirm(self, token):
	s = Serializer(current_app.config['SECRET_KEY'])
	try:
	    data = s.loads(token)
	except:
	    return False
	if data.get('confirm') != self.id:
	    return False
	self.confirmed = True
	db.session.add(self)
	db.session.commit()
	return True

    def generate_reset_token(self, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'reset': self.id})

    def reset_password(self, token, new_password):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return False
        if data.get('reset') != self.id:
            return False
        self.password = new_password
        db.session.add(self)
	db.session.commit()
        return True
	
    def ping(self):
	self.last_seen = datetime.utcnow() 
	db.session.add(self)
	db.session.commit

    def generate_email_change_token(self, new_email, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'change_email': self.id, 'new_email': new_email})

    def change_email(self, token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return False
        if data.get('change_email') != self.id:
            return False
        new_email = data.get('new_email')
        if new_email is None:
            return False
        if self.query.filter_by(email=new_email).first() is not None:
            return False
        self.email = new_email
        self.avatar_hash = hashlib.md5(
            self.email.encode('utf-8')).hexdigest()
        db.session.add(self)
	db.session.commit()
        return True


    def can(self, permissions):
	return self.role is not None and \
		(self.role.permissions & permissions) == permissions 
    
    def is_administrator(self):
	return self.can(Permission.ADMINISTER)


    #def generate_auth_token(self, expiration):
#       s = Serializer(current_app.config['SECRET_KEY'], expires_in=expiration)
#       return s.dumps({'id': self.id})

    def generate_auth_token(self):
       s = Serializer(current_app.config['SECRET_KEY'])
       return s.dumps({'id': self.id})

    @staticmethod
    def from_json(json_user):

	if json_user.get('login_type') is not None:
		return User.social_network_login(json_user)

	email = json_user.get('email')
        if User.query.filter_by(email=email).first():
                raise ValidationError('Email already registered.')

	username = json_user.get('username')
        if User.query.filter_by(username=username).first():
                #raise ValidationError('Username already in use.')
		username = User.make_unique_username(username)

	social = json_user.get('social_id')
	if User.query.filter_by(social_id=social_id).first():
                raise ValidationError('This social ID  already in use.')

	user = User(email=email, username=username, password=password)
	db.session.add(user)
	db.session.commit()
	return user


    @staticmethod
    def social_network_login(json_user):

	social_id = json_user.get('social_id')
	provider = json_user.get('login_type')

	social_access_token = json_user.get('fb_access_token')

	token_check_url = "https://graph.facebook.com/me?fields=id&access_token="+social_access_token
	resp = requests.get(token_check_url).json()
	user_id = resp.get('id')
	if user_id != social_id or social_access_token is None:
		raise ValidationError('Invalid user account')

	email = json_user.get('email')


	provider_user_id_hash = hashlib.md5(social_id.encode('utf-8')+current_app.config['SECRET_KEY']).hexdigest()
	provider_user_access_token_hash = hashlib.md5(social_access_token.encode('utf-8')+current_app.config['SECRET_KEY']).hexdigest()

	account = AccountProvider.query.filter_by(provider_user_id_hash=provider_user_id_hash, provider_user_access_token_hash=provider_user_access_token_hash).first()

	if account is not None:		
		return account.owner
	else:
		owner = User()
		owner.confirmed = True 
		db.session.add(owner)
        	db.session.commit()
		owner.add_new_social_account(provider, provider_user_id_hash, provider_user_access_token_hash)	
		return owner

    def add_new_social_account(self, provider, provider_user_id_hash, provider_user_access_token_hash):
	account = AccountProvider(owner_id = self.id, 
				provider = provider, 
				provider_user_id_hash = provider_user_id_hash, 
				provider_user_access_token_hash = provider_user_access_token_hash)

	db.session.add(account)
	db.session.commit()

    def to_json(self): 
	json_user = {
    		'url': url_for('api.get_post', id=self.id, _external=True),
     		'username': self.username,
     		'member_since': self.member_since,
     		'last_seen': self.last_seen,
     		'posts': url_for('api.get_user_posts', id=self.id, _external=True),
     		'followed_posts': url_for('api.get_user_followed_posts',id=self.id, _external=True),
		'followers': url_for('api.followers',id=self.id, _external=True),
		'posts_wanted': url_for('api.posts_wanted' ,id=self.id, _external=True),
		'post_count': self.posts.count()
        }
	return json_user

    @staticmethod
    def verify_auth_token(token):
	s = Serializer(current_app.config['SECRET_KEY']) 
	try:
		data = s.loads(token)
	except:
		return None
	return User.query.get(data['id'])


    def __repr__(self):
        return '<User %r>' % (self.username)


class AnonymousUser(AnonymousUserMixin): 
	def can(self, permissions):
		return False

	def is_administrator(self):
		return False

	def already_want(self, post):
		return False

	def is_own_post(self, post):
		return False


lm.anonymous_user = AnonymousUser


class ProductImage(db.Model):
    __tablename__ = 'productimages'
    id = db.Column(db.Integer, primary_key=True)
    filename = db.Column(db.String(100))
    order = db.Column(db.Integer)
    post_id = db.Column(db.Integer, db.ForeignKey('posts.id'))

    def __repr__(self):
	return url

class Post(db.Model):
    __tablename__ = 'posts'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(140))
    body = db.Column(db.Text())
    price = db.Column(db.Numeric, default=0)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    author_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    comments = db.relationship('Comment', backref='post', lazy='dynamic')
    product_images = db.relationship('ProductImage', backref='post', lazy='dynamic')


    #users_wanted = db.relationship('Wantit',
    #                           foreign_keys=[Wantit.post_id],
    #                           backref=db.backref('post', lazy='joined'),
    #                           lazy='dynamic',
    #                           cascade='all, delete-orphan')


    def __repr__(self):
        return '<Post %r>' % (self.body)

    @staticmethod
    def generate_fake(count=100):
	from random import seed, randint 
	import forgery_py
	
	seed()
	user_count = User.query.count() 
	for i in range(count):
            u = User.query.offset(randint(0, user_count - 1)).first()
            p = Post(body=forgery_py.lorem_ipsum.sentences(randint(1, 3)),
                     timestamp=forgery_py.date.date(True),
                     author=u)
            db.session.add(p)
            db.session.commit()

    def to_json(self, current_user):

 
	json_post = {
	    'id': self.id,
            'url': url_for('api.get_post', id=self.id, _external=True),
            'body': self.body,
	    'already_want': current_user.already_want(self),
	    'can_want': not current_user.is_own_post(self) and current_user.can(Permission.FOLLOW),
	    'can_edit': current_user.is_own_post(self),
	    'admin_edit': current_user.is_administrator(),
	    'title': self.title,
	    'seller': {"name":self.author.username, "image":self.author.avatar(size=40)},
            'timestamp': self.timestamp,
            'author': url_for('api.get_user', id=self.author_id,_external=True),
            'comments': url_for('api.get_post_comments', id=self.id, _external=True),
            'comment_count': self.comments.count(),
	    'buyers_count':self.buyers.count(),
	    'buyers': url_for('api.users_wanted', id=self.id, _external=True),
	    'images': [url_for('static', filename='media/product_images/'+image.filename, _external=True) for image in self.product_images]
        }
	return json_post

    @staticmethod
    def from_json(json_post):
	body = json_post.get('body')
	title = json_post.get('title') 
	if body is None or body == '':
		raise ValidationError('post does not have a body') 
	return Post(body=body, title=title)



    buyers = db.relationship('Wantit',
                                foreign_keys=[Wantit.post_id],
                                backref=db.backref('posts', lazy='joined'),
                                lazy='dynamic',
                                cascade='all, delete-orphan')

    tags = db.relationship('Posttag',
				foreign_keys=[Posttag.post_id],
				backref=db.backref('posts', lazy='joined'),
				lazy='dynamic',
				cascade='all, delete-orphan')




class Comment(db.Model):
	__tablename__ = 'comments'
	id = db.Column(db.Integer, primary_key=True)
	body = db.Column(db.Text)
	body_html = db.Column(db.Text)
	timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow) 
	disabled = db.Column(db.Boolean)
	author_id = db.Column(db.Integer, db.ForeignKey('users.id'))
	post_id = db.Column(db.Integer, db.ForeignKey('posts.id'))
    
	@staticmethod
	def on_changed_body(target, value, oldvalue, initiator): 
		allowed_tags = ['a', 'abbr', 'acronym', 'b', 'code', 'em', 'i', 'strong']

        	target.body_html = bleach.linkify(bleach.clean(
				markdown(value, output_format='html'),
            			tags=allowed_tags, strip=True))

	def to_json(self):
        	json_comment = {
            		'url': url_for('api.get_comment', id=self.id, _external=True),
            		'post': url_for('api.get_post', id=self.post_id, _external=True),
            		'body': self.body,
            		'body_html': self.body_html,
            		'timestamp': self.timestamp,
            		'author': url_for('api.get_user', id=self.author_id,_external=True)
       		}
        	return json_comment


	@staticmethod
    	def from_json(json_comment):
        	body = json_comment.get('body')
        	if body is None or body == '':
            		raise ValidationError('comment does not have a body')
        	return Comment(body=body)


db.event.listen(Comment.body, 'set', Comment.on_changed_body)



#if enable_search:
#    whooshalchemy.whoosh_index(current_app, Post)
